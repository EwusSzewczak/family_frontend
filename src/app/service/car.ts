export class Car{
  id: number;
  marka: string;
  model: string;
  numer_rejestracyjny: string;
  czlonek_samochod_id: number;
  constructor(id: number, marka: string, model: string, numer_rejestracyjny: string, czlonek_samochod_id: number) {
    this.id = id;
    this.marka = marka;
    this.model = model;
    this.numer_rejestracyjny = numer_rejestracyjny;
    this.czlonek_samochod_id = czlonek_samochod_id;
  }
}
