import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Rejestracja} from './rejestracja';
import {FormControl, FormGroup} from '@angular/forms';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-rejestracja',
  templateUrl: './rejestracja.component.html',
  styleUrls: ['./rejestracja.component.css']
})
export class RejestracjaComponent implements OnInit {
  user: Rejestracja = new Rejestracja('', '', '', '');
  imie = new FormControl('', Validators.required);
  message: any;
  httpData: any;
  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
  }

  public register(): void{
    this.httpClient.post('http://localhost:8080/czlonek', this.user).subscribe((data) => this.message = data);
  }

}
