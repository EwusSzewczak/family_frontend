export class Rejestracja{
  imie: string;
  nazwisko: string;
  email: string;
  haslo: string;
  constructor(imie: string, nazwisko: string, email: string, haslo: string) {
    this.imie = imie;
    this.nazwisko = nazwisko;
    this.email = email;
    this.haslo = haslo;
  }
}
