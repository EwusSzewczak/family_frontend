import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RejestracjaComponent} from './rejestracja/rejestracja.component';
import {CarComponent} from './car/car.component';
import {LogowanieComponent} from './logowanie/logowanie.component';

const routes: Routes = [
  {path: 'rejestracja', component: RejestracjaComponent},
  {path: 'samochody', component: CarComponent},
  {path: 'logowanie', component: LogowanieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
